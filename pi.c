#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/**
 * @brief Generate a uniform random number bewteen 0 and 1
 *
 * @return		The random number
 */
long double longRand () {
	return (long double)rand() / (long double)RAND_MAX;
}

/**
 * @brief Computes the factorial of a number.
 *
 * @param f		Number to get the factorial off.
 * @return		The factorial of 'f'
 */
long fact(unsigned long num)
{
	if ( num == 0 )
		return 1;

	return num * fact(num - 1);
}

/**
 * @brief Computes pi using the Leibniz infinite series.
 *
 * @param accuracy	The number of cycles to run.
 * @return			Pi
 */
long double leibniz (int accuracy) {
	long double output = 0;

	for (int i = 0; i < accuracy; i++) {
		int op = -1;

		if (i % 2 == 0)
			op = 1;

		output += op * 4.00 / (2.00 * i + 1.00 );
	}

	return output;
}

/**
 * @brief Computes pi using a Monte Carlo method.
 *
 * @param accuracy	The number of cycles to run.
 * @return			Pi
 */
long double monteCarlo (int accuracy) {
	int insideCount = 0;

	long double x, y, len;

	for (int i = 0; i < accuracy; i++) {
		x = longRand();
		y = longRand();

		len = sqrt(x*x + y*y);

		if (len <= 1.00)
			insideCount++;
	}

	return (long double)insideCount * 4.00 / (long double)accuracy;
}

/**
 * @brief Computes pi using a Monte Carlo method in 3 dimentions.
 *
 * @param accuracy	The number of cycles to run.
 * @return			Pi
 */
long double monteCarlo3d (int accuracy) {
	int insideCount = 0;

	long double x, y, z, len;

	for (int i = 0; i < accuracy; i++) {
		x = longRand();
		y = longRand();
		z = longRand();

		len = sqrt(x*x + y*y + z*z);

		if (len <= 1.00)
			insideCount++;
	}

	return (long double)insideCount * 8.00 * (3.00 / 4.00) / (long double)accuracy;
}

/**
 * @brief Computes pi using the Chudnovsky infinite series.
 *
 * @param accuracy	The number of cycles to run.
 * @return			Pi
 */
long double chudnovsky (int accuracy) {
	// Required constants
	int a = 640320;
	int b = 13591409;
	int c = 545140134;

	long double output = 0;

	long numerator, denominator;

	for (int i = 0; i < accuracy; i++) {
		numerator = fact(6 * i) * (b + c * i);
		denominator = fact(3 * i) * pow(fact(i), 3) * pow(-1 * a, 3 * i);

		output += (long double)numerator / (long double)denominator;
	}

	output *= 12.00 / sqrt(pow(a, 3));

	return 1.00 / output;
}

/**
 * @brief Computes pi using the BPP algorithm.
 *
 * @param accuracy	The number of cycles to run.
 * @return			Pi
 */
long double bpp (int accuracy) {
	long double a, b, c, d, e, output;

	output = 0;

	for (int i = 0; i < accuracy; i++) {
		a = 1.00 / pow(16, i);
		b = 4.00 / (8 * i + 1);
		c = 2.00 / (8 * i + 4);
		d = 1.00 / (8 * i + 5);
		e = 1.00 / (8 * i + 6);

		output += a * (b - c - d - e);
	}

	return output;
}

/**
 * @brief Outputs the usage help.
 */
void printUsage(char *command) {
	printf("Usage: %s <algorithm> <accuracy>\n", command);
	printf("\n");
	printf("\t<algorithm>\n");
	printf("\t\t0: Precomputed\n");
	printf("\t\t1: Leibniz\n");
	printf("\t\t2: Monte Carlo\n");
	printf("\t\t3: Monte Carlo 3D\n");
	printf("\t\t4: Chudnovsky\n");
	printf("\t\t5: Bailey–Borwein–Plouffe (BPP)\n");
	printf("\n");
	printf("\t<accuracy>\n");
	printf("\t\tThe number of cycles to run.\n");
	printf("\t\tNote: not applicable to precomputed.\n");
}

int main (int argc, char *argv[]) {
	// 16 digits
	long double pi = 3.141592653589793;

	// Seed rand
	srand(time(NULL));

	if (argc < 2 | argc > 3) {
		printUsage(argv[0]);

		return 0;
	}

	int type, accuracy;

	if (sscanf(argv[1], "%i", &type) != 1) {
		printUsage(argv[0]);

		return 0;
	}

	if (type == 0) {
		printf("%.15Lf\n", pi);

		return 0;
	}

	if (argc < 3 || sscanf(argv[2], "%i", &accuracy) != 1) {
		printUsage(argv[0]);

		return 0;
	}

	switch(type) {
		case 1:
			printf("%.15Lf\n", leibniz(accuracy));
			break;

		case 2:
			printf("%.15Lf\n", monteCarlo(accuracy));
			break;

		case 3:
			printf("%.15Lf\n", monteCarlo3d(accuracy));
			break;

		case 4:
			printf("%.15Lf\n", chudnovsky(accuracy));
			break;

		case 5:
			printf("%.15Lf\n", bpp(accuracy));
			break;

		default:
			printUsage(argv[0]);
	}


	return 0;
}
