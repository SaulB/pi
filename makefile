CC=gcc
CFLAGS=-lm.

build:
	$(CC) -o pi pi.c -lm

clean:
	-rm -f pi

rebuild: clean build
