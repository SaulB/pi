# Pi

Different methods of calculating pi.

## Usage

```
Usage: ./pi <algorithm> <accuracy>

	<algorithm>
		0: Precomputed
		1: Leibniz
		2: Monte Carlo
		3: Monte Carlo 3D
		4: Chudnovsky
		5: Bailey–Borwein–Plouffe (BPP)

	<accuracy>
		The number of cycles to run.
		Note: not applicable to precomputed.
```

## Build

`make`

### Clean

`make clean`

### Rebuild

`make rebuild`
